#pragma once

#include <QObject>

class QNetworkAccessManager;

namespace IOML
{
  class NetworkAccessManager : public QObject
  {
    Q_OBJECT
  public:
    NetworkAccessManager();
    ~NetworkAccessManager();
  public:
    QNetworkAccessManager* manager() const { return m_nam; }
  private:
    QNetworkAccessManager* m_nam;
  };
}
