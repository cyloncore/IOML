package com.cyloncore.ioml;

import java.lang.String;

import android.content.Intent;
import android.util.Log;

import org.qtproject.qt5.android.QtNative;

public class IOMLObject
{
  public static boolean shareText(String text)
  {
  
    if (QtNative.activity() == null)
    {
      Log.d("IOML", "No QtNative activity!");
      return false;
    }
    
    Intent sendIntent = new Intent();
    sendIntent.setAction(Intent.ACTION_SEND);
    sendIntent.putExtra(Intent.EXTRA_TEXT, text);
    sendIntent.setType("text/plain");

    // Verify that the intent will resolve to an activity
    if (sendIntent.resolveActivity(QtNative.activity().getPackageManager()) != null) {
      QtNative.activity().startActivity(sendIntent);
      return true;
    } else {
      Log.d("IOML", "Intent not resolved!");
    }
    return false;
  }
}
