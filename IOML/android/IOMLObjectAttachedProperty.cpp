#include "../IOMLObjectAttachedProperty.h"

#include <QDebug>

#include <QtAndroid>
#include <QAndroidJniEnvironment>
#include <QAndroidJniObject>

using namespace IOML;

struct IOMLObjectAttachedProperty::Private
{
};

IOMLObjectAttachedProperty::IOMLObjectAttachedProperty(QObject* _parent) : QObject(_parent), d(new Private)
{
}

IOMLObjectAttachedProperty::~IOMLObjectAttachedProperty()
{
}

void IOMLObjectAttachedProperty::shareText(const QString& _text)
{
  QAndroidJniObject jniText = QAndroidJniObject::fromString(_text);
  jboolean result = QAndroidJniObject::callStaticMethod<jboolean>("com/cyloncore/ioml/IOMLObject", "shareText", "(Ljava/lang/String;)Z", jniText.object<jstring>());
  QAndroidJniEnvironment()->ExceptionClear();
  if(not result)
  {
    qWarning() << "Failed to share text: " << _text;
  }
}
