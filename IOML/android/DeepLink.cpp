#include "../DeepLink_p.h"

#include <QDebug>
#include <QtAndroid>
#include <QAndroidJniEnvironment>
#include <QAndroidJniObject>

using namespace IOML;

DeepLink::DeepLink() : d(new Private)
{
  QAndroidJniObject qt_activity = QtAndroid::androidActivity();
  
  // Intent intent = getIntent();
  QAndroidJniObject intent = qt_activity.callObjectMethod("getIntent", "()Landroid/content/Intent;");
  
  // String action = intent.getAction();
  QAndroidJniObject action = intent.callObjectMethod("getAction", "()Ljava/lang/String;");
  if(action.isValid())
  {
    d->action = action.toString();
  }
  
  // Uri data = intent.getData();
  QAndroidJniObject uri = intent.callObjectMethod("getData", "()Landroid/net/Uri;");
  if(uri.isValid())
  {
    d->url    = uri.callObjectMethod("toString", "()Ljava/lang/String;").toString();
  }
}
