#include "NetworkRequest.h"

#include <QDebug>

#include <QNetworkAccessManager>
#include <QNetworkReply>

#include "NetworkAccessManager.h"

using namespace IOML;

NetworkRequest::NetworkRequest(QObject* _parent) : QObject(_parent)
{
}

NetworkRequest::~NetworkRequest()
{
}

void NetworkRequest::get()
{
  if(m_manager)
  {
    handleReply(m_manager->manager()->get(createRequest()));
  } else {
    m_result.clear();
    m_error = "No network manager!";
    emit(resultChanged());
    emit(errorChanged());
  }
}

void NetworkRequest::post(const QByteArray& _data)
{
  if(m_manager)
  {
    handleReply(m_manager->manager()->post(createRequest(), _data));
  } else {
    m_result.clear();
    m_error = "No network manager!";
    emit(resultChanged());
    emit(errorChanged());
  }
}

QNetworkRequest NetworkRequest::createRequest()
{
  QNetworkRequest r;
  r.setUrl(m_url);
  for(QVariantMap::iterator it = m_headers.begin(); it != m_headers.end(); ++it)
  {
    r.setRawHeader(it.key().toLatin1(), it.value().toString().toUtf8());
  }
  
  return r;
}

void NetworkRequest::handleReply(QNetworkReply* _reply)
{
  if(m_reply)
  {
    m_reply->abort();
    m_reply->deleteLater();
  }
  m_reply = _reply;
  connect(m_reply, &QNetworkReply::finished, this, &NetworkRequest::replyFinished);
}

void NetworkRequest::replyFinished()
{
  m_result = m_reply->readAll();
  emit(resultChanged());
  m_error = m_reply->error() == QNetworkReply::NoError ? QString() : m_reply->errorString();
  emit(errorChanged());
  m_reply->deleteLater();
  m_reply = nullptr;
}
