#pragma once

#include <QUrl>
#include <QQmlPropertyMap>
#include <QNetworkReply>

class QNetworkRequest;

namespace IOML
{
  class NetworkAccessManager;
  class NetworkRequest : public QObject
  {
    Q_OBJECT
    Q_PROPERTY(QUrl url MEMBER m_url NOTIFY urlChanged)
    Q_PROPERTY(QVariantMap headers MEMBER m_headers NOTIFY headersChanged)
    Q_PROPERTY(IOML::NetworkAccessManager* manager MEMBER m_manager NOTIFY managerChanged)
    Q_PROPERTY(QByteArray result READ result NOTIFY resultChanged)
    Q_PROPERTY(QString error READ error NOTIFY errorChanged)
  public:
    NetworkRequest(QObject* _parent = nullptr);
    ~NetworkRequest();
    Q_INVOKABLE void get();
    Q_INVOKABLE void post(const QByteArray& _data);
    QByteArray result() const { return m_result; }
    QString error() const { return m_error; }
  private:
    QNetworkRequest createRequest();
    void handleReply(QNetworkReply* _reply);
  signals:
    void urlChanged();
    void headersChanged();
    void managerChanged();
    void resultChanged();
    void errorChanged();
  private slots:
    void replyFinished();
  private:
    QUrl m_url;
    QVariantMap m_headers;
    IOML::NetworkAccessManager* m_manager = nullptr;
    QByteArray m_result;
    QString m_error;
    QNetworkReply* m_reply = nullptr;
  };
}
