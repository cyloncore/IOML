#pragma once

#include <QObject>

#include <QUrl>

class QFileSystemWatcher;

namespace IOML
{
  class File : public QObject
  {
    Q_OBJECT
    Q_PROPERTY(QString content READ content WRITE setContent NOTIFY contentChanged)
    Q_PROPERTY(QUrl url READ url WRITE setUrl NOTIFY urlChanged)
    Q_PROPERTY(QString fileName READ fileName WRITE setFileName NOTIFY fileNameChanged)
    Q_PROPERTY(bool exists READ exists NOTIFY existsChanged)
    Q_PROPERTY(bool monitorChanges READ monitorChanges WRITE setMonitorChanges NOTIFY monitorChangesChanged)
  public:
    explicit File(QObject *parent = 0);
    ~File();
    Q_INVOKABLE void readFile();
    Q_INVOKABLE void readFile(const QUrl& _url);
    Q_INVOKABLE void writeFile();
    Q_INVOKABLE void writeFile(const QUrl& _url);
    /**
     * @return true if the file exists
     */
    bool exists() const;
    QString content() const { return m_content; }
    void setContent(const QString& _content) { if(m_content != _content) { m_content = _content; emit(contentChanged()); } }
    QUrl url() const { return m_url; }
    void setUrl(const QUrl& _url);
    bool monitorChanges() const { return m_monitorChanges; }
    void setMonitorChanges(bool _v);
    QString fileName() const { return m_fileName; }
    void setFileName(const QString& _filename);
  private slots:
    void fileChanged(const QString& _name);
  signals:
    void contentChanged();
    void urlChanged();
    void monitorChangesChanged();
    void fileChanged();
    void fileNameChanged();
    void existsChanged();
  private:
    QString m_content, m_fileName;
    QUrl m_url;
    bool m_monitorChanges = false;
    static QFileSystemWatcher* s_watcher;
  };
}
