#pragma once

#include <QObject>

#include <QUrl>

namespace IOML
{
  class DeepLink : public QObject
  {
    Q_OBJECT
    Q_PROPERTY(QUrl url READ url CONSTANT)
    Q_PROPERTY(QString urlFragment READ urlFragment CONSTANT)
    Q_PROPERTY(QString action READ action CONSTANT)
  public:
    DeepLink();
    ~DeepLink();
    QUrl url() const;
    QString action() const; 
  private:
    QString urlFragment() const;
    struct Private;
    Private* const d;
  };
}
