#pragma once

#include <QObject>

namespace IOML
{
  class IOMLObjectAttachedProperty;
  class IOMLObject : public QObject
  {
    Q_OBJECT
  public:
    static IOMLObjectAttachedProperty* qmlAttachedProperties(QObject *object);
  };
}

#include <QtQml>
QML_DECLARE_TYPEINFO(IOML::IOMLObject, QML_HAS_ATTACHED_PROPERTIES)
