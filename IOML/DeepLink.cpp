#include "DeepLink_p.h"

using namespace IOML;

DeepLink::~DeepLink()
{
  delete d;
}

QUrl DeepLink::url() const
{
  return d->url;
}

QString DeepLink::urlFragment() const
{
  return d->url.fragment();
}

QString DeepLink::action() const
{
  return d->action;
}
