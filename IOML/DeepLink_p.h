#include "DeepLink.h"

namespace IOML
{
  struct DeepLink::Private
  {
    QUrl url;
    QString action;
  };
}
