#include "IOMLObjectAttachedProperty.h"

#include <qqml.h>
#include <QClipboard>
#include <QDir>
#include <QFileInfo>
#include <QFile>
#include <QGuiApplication>
#include <QQmlEngine>

using namespace IOML;

void IOMLObjectAttachedProperty::textToClipboard(const QString& _text)
{
  QGuiApplication::clipboard()->setText(_text);
}

void IOMLObjectAttachedProperty::addImportPath(const QUrl& _url)
{
  qmlEngine(this)->addImportPath(QFileInfo(_url.toLocalFile()).absolutePath());
}

void IOMLObjectAttachedProperty::setCurrentPath(const QUrl& _url)
{
  QDir::setCurrent(QFileInfo(_url.toLocalFile()).absolutePath());
}

void IOMLObjectAttachedProperty::moveFile(const QString& _from, const QString& _to)
{
  QFile::rename(_from, _to);
}
