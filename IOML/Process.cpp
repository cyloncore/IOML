#include "Process.h"

#include <QDebug>
#include <QProcess>

Process::Process() : m_process(nullptr)
{
}

Process::~Process()
{
  delete m_process;
}

void Process::start()
{
  if(m_process and m_process->state() != QProcess::NotRunning)
  {
    qWarning() << "Process has already been started";
    return;
  }
  delete m_process;
  m_process = new QProcess;
  connect(m_process, SIGNAL(stateChanged(QProcess::ProcessState)), this, SIGNAL(stateChanged()));
  connect(m_process, SIGNAL(readyReadStandardError()), this, SLOT(readStandardError()));
  connect(m_process, SIGNAL(readyReadStandardOutput()), this, SLOT(readStandardOutput()));
  m_process->start(m_program, m_arguments);
}

Process::State Process::state() const
{
  if(m_process)
  {
    switch(m_process->state())
    {
      case QProcess::NotRunning:
        return State::NotRunning;
      case QProcess::Starting:
        return State::Starting;
      case QProcess::Running:
        return State::Running;
    }
    return State::NotRunning;
  } else {
    return State::NotRunning;
  }
}

void Process::kill()
{
  if(m_process == nullptr or m_process->state() == QProcess::NotRunning)
  {
    qWarning() << "Process is not running";
    return;
  }
  m_process->kill();
}

void Process::terminate()
{
  if(m_process == nullptr or m_process->state() == QProcess::NotRunning)
  {
    qWarning() << "Process is not running";
    return;
  }
  m_process->terminate();
}

void Process::readStandardError()
{
  QString str = m_process->readAllStandardError();
  m_standardError  += str;
  m_output         += str;
  emit(standardErrorChanged());
  emit(outputChanged());
}

void Process::readStandardOutput()
{
  QString str = m_process->readAllStandardOutput();
  m_standardOutput += str;
  m_output         += str;
  emit(standardOutputChanged());
  emit(outputChanged());
}

void Process::write(const QString &_text)
{
  if(m_process == 0 or m_process->state() == QProcess::NotRunning)
  {
    qWarning() << "Process not yet started";
    return;
  }
  m_process->write(_text.toUtf8());
}



