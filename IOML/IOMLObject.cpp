#include "IOMLObject.h"

#include "IOMLObjectAttachedProperty.h"

using namespace IOML;

IOMLObjectAttachedProperty* IOMLObject::qmlAttachedProperties(QObject* object)
{
  return new IOMLObjectAttachedProperty(object);
}
