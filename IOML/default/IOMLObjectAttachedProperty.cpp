#include "../IOMLObjectAttachedProperty.h"

using namespace IOML;

struct IOMLObjectAttachedProperty::Private
{
};

IOMLObjectAttachedProperty::IOMLObjectAttachedProperty(QObject* _parent) : QObject(_parent), d(new Private)
{
}

IOMLObjectAttachedProperty::~IOMLObjectAttachedProperty()
{
}

void IOMLObjectAttachedProperty::shareText(const QString& _text)
{
  textToClipboard(_text);
}
