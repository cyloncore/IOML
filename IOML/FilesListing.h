#include <QObject>

namespace IOML
{
  class FilesListing : public QObject
  {
    Q_OBJECT
    Q_PROPERTY(QString directoryName READ directoryName WRITE setDirectoryName NOTIFY directoryNameChanged)
    Q_PROPERTY(QStringList files READ files NOTIFY filesChanged)
    Q_PROPERTY(QStringList nameFilters READ nameFilters WRITE setNameFilters NOTIFY nameFiltersChanged)
  public:
    explicit FilesListing(QObject *parent = 0);
    ~FilesListing();
    Q_INVOKABLE void reload();
  public:
    void setDirectoryName(const QString& _directoryName);
    QString directoryName() const { return m_directoryName; }
    QStringList files() const { return m_files; }
    QStringList nameFilters() const { return m_nameFilters; }
    void setNameFilters(const QStringList& _nameFilters);
  signals:
    void directoryNameChanged();
    void filesChanged();
    void nameFiltersChanged();
  private:
    QString m_directoryName;
    QStringList m_files, m_nameFilters;
  };
}
