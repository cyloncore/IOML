#include "File.h"

#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QFileSystemWatcher>

using namespace IOML;

QFileSystemWatcher* File::s_watcher = nullptr;

File::File(QObject *parent) :
  QObject(parent)
{
}

File::~File()
{
}

void File::readFile(const QUrl& _url)
{
  setUrl(_url);
  readFile();
}
void File::readFile()
{
  QString fn = m_fileName.isEmpty() ? m_url.toLocalFile() : m_fileName;
  QFile file(fn);
  file.open(QIODevice::ReadOnly);
  setContent(file.readAll());
}

void File::writeFile(const QUrl& _url)
{
  setUrl(_url);
  writeFile();
}

void File::writeFile()
{
  QString fn = m_fileName.isEmpty() ? m_url.toLocalFile() : m_fileName;
  QFile file(fn);
  file.open(QIODevice::WriteOnly);
  file.write(m_content.toUtf8());
}

bool File::exists() const
{
  QString fn = m_fileName.isEmpty() ? m_url.toLocalFile() : m_fileName;
  return QFileInfo::exists(fn);
}

void File::setUrl(const QUrl& _url)
{
  if(_url == m_url) return;
  if(m_monitorChanges)
  {
    if(not m_fileName.isEmpty())
    {
      s_watcher->removePath(m_fileName);
    }
    if(not m_url.toLocalFile().isEmpty())
    {
      s_watcher->removePath(m_url.toLocalFile());
    }
    if(not _url.toLocalFile().isEmpty())
    {
      s_watcher->addPath(_url.toLocalFile());
    }
  }
  m_fileName.clear();
  emit(fileNameChanged());
  m_url = _url;
  emit(urlChanged());
  emit(existsChanged());
}

void File::setMonitorChanges(bool _v)
{
  if(m_monitorChanges == _v) return;
  if(_v)
  {
    if(not s_watcher)
    {
      s_watcher = new QFileSystemWatcher;
    }
    QObject::connect(s_watcher, SIGNAL(fileChanged(const QString&)), this, SLOT(fileChanged(const QString&)));
    if(not m_url.toLocalFile().isEmpty())
    {
      s_watcher->addPath(m_url.toLocalFile());
    }
  } else {
    QObject::disconnect(s_watcher, SIGNAL(fileChanged(const QString&)), this, SLOT(fileChanged(const QString&)));
    if(not m_url.toLocalFile().isEmpty())
    {
      s_watcher->removePath(m_url.toLocalFile());
    }
  }
  m_monitorChanges = _v;
  emit(monitorChangesChanged());
}

void File::setFileName(const QString& _filename)
{
  if(m_monitorChanges)
  {
    if(not m_fileName.isEmpty())
    {
      s_watcher->removePath(m_fileName);
    }
    if(not m_url.toLocalFile().isEmpty())
    {
      s_watcher->removePath(m_url.toLocalFile());
    }
    if(not m_fileName.isEmpty())
    {
      s_watcher->addPath(m_fileName);
    }
  }
  m_fileName = _filename;
  emit(fileNameChanged());
  m_url.clear();
  emit(urlChanged());
  emit(existsChanged());
}


void File::fileChanged(const QString& _name)
{
  if(_name == m_url.toLocalFile())
  {
    emit(fileChanged());
    emit(existsChanged());
  }
}

#include "moc_File.cpp"
