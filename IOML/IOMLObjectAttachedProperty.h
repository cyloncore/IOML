#include <QObject>

namespace IOML
{
  class IOMLObjectAttachedProperty : public QObject
  {
    Q_OBJECT
  public:
    IOMLObjectAttachedProperty(QObject* _parent = nullptr);
    ~IOMLObjectAttachedProperty();
    Q_INVOKABLE void shareText(const QString& _text);
    Q_INVOKABLE void textToClipboard(const QString& _text);
    Q_INVOKABLE void addImportPath(const QUrl& _url);
    Q_INVOKABLE void setCurrentPath(const QUrl& _url);
    Q_INVOKABLE void moveFile(const QString& _from, const QString& _to);
  private:
    struct Private;
    Private* const d;
  };
}

