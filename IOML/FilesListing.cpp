#include "FilesListing.h"

#include <QDir>

using namespace IOML;

FilesListing::FilesListing(QObject *parent) : QObject(parent)
{
}

FilesListing::~FilesListing()
{
}

void FilesListing::reload()
{
  QDir dir(m_directoryName);
  dir.setNameFilters(m_nameFilters);
  m_files = dir.entryList();
  emit(filesChanged());
}

void FilesListing::setDirectoryName(const QString& _directoryName)
{
  m_directoryName = _directoryName;
  emit(directoryNameChanged());
  reload();
}

void FilesListing::setNameFilters(const QStringList& _nameFilters)
{
  m_nameFilters = _nameFilters;
  emit(nameFiltersChanged());
  reload();
}
