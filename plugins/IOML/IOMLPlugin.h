#pragma once

#include <QQmlExtensionPlugin>

class IOMLPlugin: public QQmlExtensionPlugin {
  Q_OBJECT
  Q_PLUGIN_METADATA(IID "IOML/1.0")
public:
  
  void registerTypes(const char* uri) override;
};
