#include "IOMLPlugin.h"

#include <QtQml>

#include <IOML/DeepLink.h>
#include <IOML/File.h>
#include <IOML/FilesListing.h>
#include <IOML/NetworkAccessManager.h>
#include <IOML/NetworkRequest.h>
#include <IOML/Process.h>
#include <IOML/IOMLObject.h>
#include <IOML/IOMLObjectAttachedProperty.h>

using namespace IOML;

void IOMLPlugin::registerTypes(const char* uri)
{
  Q_ASSERT(uri == QLatin1Literal("IOML"));
  qmlRegisterType<DeepLink>(uri, 1, 0, "DeepLink");
  qmlRegisterType<File>(uri, 1, 0, "File");
  qmlRegisterType<FilesListing>(uri, 1, 0, "FilesListing");
  qmlRegisterType<IOMLObject>(uri, 1, 0, "IOML");
  qmlRegisterType<NetworkRequest>(uri, 1, 0, "NetworkRequest");
  qmlRegisterType<NetworkAccessManager>(uri, 1, 0, "NetworkAccessManager");
  qmlRegisterType<Process>(uri, 1, 0, "Process");
}
